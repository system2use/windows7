Preparing target host :
=======================

First, you must install the following softwares :

[Chocolatey](https://chocolatey.org/) :
---------------------------------------

```powershell
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%systemdrive%\chocolatey\bin
```

[Cygwin](http://cygwin.com/install.html) :
------------------------------------------

Use [this link](http://cygwin.com/setup-x86.exe) to download the Cygwin installer, then run it.

[Xming](http://sourceforge.net/projects/xming/) :
-------------------------------------------------

Use [this link](http://downloads.sourceforge.net/project/xming/Xming/6.9.0.31/Xming-6-9-0-31-setup.exe?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fxming%2F&ts=1403392761&use_mirror=freefr) to download Xming, the X11 legacy on Windows, then install it.

Availible commands in DOS path :
================================

* sudo : Execute any command using priviliges.
* cyg-install : Install new packages on Cygwin.
* setup-ipv6 : Setup IPv6 teredo over IPv4.

Optional softwares to use :
===========================

You can either install all the stuff you need daily using [Ninite](http://ninite.com/), or check the list below :

[Vagrant](http://vagrantup.com/) :
------------------------------------------

Use [this link](https://dl.bintray.com/mitchellh/vagrant/vagrant_1.6.3.msi) to download the Vagrant package, then install it.

[VirtualBox](http://virtualbox.org/) :
------------------------------------------

Use [this link](http://download.virtualbox.org/virtualbox/4.3.12/VirtualBox-4.3.12-93733-Win.exe) to download the VirtualBox package, then install it with the [Extensions pack](http://download.virtualbox.org/virtualbox/4.3.12/Oracle_VM_VirtualBox_Extension_Pack-4.3.12-93733.vbox-extpack).
