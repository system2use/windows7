export nodosfilewarning=1

export RONIN_WIN_DRIVE="c"
export RONIN_WIN_DIR="/cygdrive/"$RONIN_WIN_DRIVE

export RONIN_CYGWIN_DIR=$RONIN_WIN_DIR"/Cygwin"
export RONIN_CHOCO_DIR=$RONIN_WIN_DIR"/Chocolatey"

export RONIN_BOOT_DIR=$RONIN_ROOT"/boot/os/windows"
export RONIN_DOS_DIR=$RONIN_BOOT_DIR"/dos"

export RONIN_PATH=$RONIN_PATH":"$RONIN_CYGWIN_DIR"/bin"
export RONIN_PATH=$RONIN_PATH":"$RONIN_CHOCO_DIR"/bin"

