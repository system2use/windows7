alias cyg-install=$RONIN_CYGWIN_DIR"/cygwinsetup.exe -q -P"
alias choco-install=$RONIN_CHOCO_DIR"/bin/chocolatey.exe install"

export CHOCO_PKGs=""

export CYG_PKGs="wget,curl,doxygen,gcc-ada,gcc-g++,gcc-java,make,ncurses,upx"
export CYG_PKGs=$CYG_PKGs",openssl,openssl-devel"
export CYG_PKGs=$CYG_PKGs",gcr,geoip,ipcalc,iperf,netcat,nettle,opencdk"

